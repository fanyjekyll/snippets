## javascript scraper

a js equivalent of web scraper usually in php. this was written for a jekyll website that would otherwise have to use an iframe to embed the pulled picture.

what is does

* php scrapes a random (one of the X newest ..) images on remote website. (this needs occasional cron)

* stores the SRC of the image in JSON file so that the scraper does not happen every time someone loads the website

* js parses that JSON and appends image


